import {
    gql
} from 'apollo-boost';


const getProductsQuery = gql `
{
    allProducts{
        name
        id
    }
}
`;

const getUsersQuery = gql `
{
    allUsers{
        firstName
        id
        email
    }
}
`;

export {
    getProductsQuery,
    getUsersQuery
};