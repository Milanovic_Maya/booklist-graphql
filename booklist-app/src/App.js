import React, { Component } from 'react';

// components
import ProductList from './components/ProductList';
import AddProduct from './components/AddProduct';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// apollo client setup
const client = new ApolloClient({
  // get data from here
  uri: 'https://fakerql.com/graphql'
});

class App extends Component {
  render() {
    return (
      // provider injects data into application
      <ApolloProvider client={client}>
        <div className="app">
          <h1>Groceries: </h1>
          <ProductList />
          <AddProduct/>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
