import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import { getUsersQuery } from '../queries/Queries';

class AddProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            price: '',
            userId: ''
        };
    };

    displayUsers = () => {
        const { data: { allUsers, loading, error } } = this.props;
        if (loading) return <option>Loading users</option>
        if (error) return <option>Error!</option>

        return (
            allUsers.map(user => (
                <option value={user.id} key={user.id}>
                    {user.firstName}
                </option>
            ))
        );

    }

    onNameChange = e => this.setState({ name: e.target.value })
    onPriceChange = e => this.setState({ price: e.target.value })
    onUserSelect = e => this.setState({ userId: e.target.value })


    onFormSubmit = e => {
        e.preventDefault();

        console.log(this.state);
        
    }

    render() {

        const { name, price } = this.state;

        return (
            <div className="container">
                <form id="add-product" onSubmit={this.onFormSubmit}>
                    <div className="field">
                        <label>Product name:</label>
                        <input
                            type="text"
                            onChange={this.onNameChange}
                            value={name}
                        />
                    </div>
                    <div className="field">
                        <label>Price:</label>
                        <input
                            type="text"
                            onChange={this.onPriceChange}
                            value={price}
                        />
                    </div>
                    <div className="field">
                        <label>User:</label>
                        <select onChange={this.onUserSelect}>
                            <option>Select user</option>
                            {this.displayUsers()}
                        </select>
                    </div>
                    <button>+</button>
                </form>
            </div>
        );
    }
}

// bind query to component to have access to data from query

export default graphql(getUsersQuery)(AddProduct);