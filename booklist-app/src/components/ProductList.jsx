import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import { getProductsQuery } from '../queries/Queries';


class ProductList extends Component {

    displayProducts = () => {
        const { data: { allProducts, loading, error } } = this.props;

        if (loading) return <div>LOADING POSTS...</div>
        if (error) return <div>ERROR!</div>
        return allProducts.map(product => (
            <li key={product.id}>{product.name}</li>
        ))
    }

    render() {

        return (
            <div className="container">
                <ul className="product-list">
                    {this.displayProducts()}
                </ul>
            </div>
        );
    }
}

// bind query to component to have access to data from query

export default graphql(getProductsQuery)(ProductList);